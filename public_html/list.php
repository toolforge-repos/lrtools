<?php

require_once('config.php');
$dbname = 'enwiki_p';
$dbhost = 'enwiki.labsdb';
$con = new mysqli($dbhost, $config['user'], $config['pass'], $dbname);
if (mysqli_connect_errno()) {
	printf("Connect failed: %s\n", mysqli_connect_error());
	exit();
}
$query = "SELECT site_global_key FROM sites where site_global_key <> 'enwiki';";
$result = $con->query($query);
while($row = $result->fetch_row()) {
	$wikis[] = $row[0];
}
$c = '<div style="margin:30px; text-align:center;">
Wiki: <select id="wiki">
<option selected>enwiki</option>';

foreach($wikis as $value) {
  $c .= "<option>$value</option>";
}
$c .= '</select>';
$c .= 'User: <input id="user"/>
Namespace: <select id="namespace">
<option value="0">Main</option>
</select>
<input type="button" id="ok" style="padding: 1 15px;" value="OK" />
</div>';
@$content['scripts'] .= '<script type="text/javascript" src="//tools-static.wmflabs.org/cdnjs/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript" src="/' . $config['tool'] .'/js/list.js"></script>
	';
?>
