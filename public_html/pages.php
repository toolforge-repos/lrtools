<?php
require_once('config.php');
/*
config.php:
$config['user']='dbuser';
$config['pass']='dbpass';
$config['tool']='Name of tool';
*/
$content['title'] = 'PageList *** LRTools';
$content['title2'] = "<a href=\"/{$config['tool']}/pages/\">PageList</a>";

if(isset($_GET['dbname']) && isset($_GET['user'])) {
	$content['scripts'] = str_replace('{$home}', $config['tool'], file_get_contents('headsorter.tpl'));
	$dbname = 'enwiki_p';
	$dbhost = 'enwiki.labsdb';
	$con = new mysqli($dbhost, $config['user'], $config['pass'], $dbname);
	if (mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		exit();
	}
	$db = $con->real_escape_string($_GET['dbname']);
	if(substr($db, -2) == '_p')
		$db = substr($db, 0, -2);
	$query = "SELECT site_data FROM sites WHERE site_global_key='{$db}'";
	$result = $con->query($query);
	if($result->num_rows > 0) {
		while($row = $result->fetch_row()) {
			$urldata = $row[0];
		}
		$urldata = unserialize($urldata);
		$filepath = str_replace('$1', '', $urldata['paths']['file_path']);
		$pagepath = str_replace('$1', '', $urldata['paths']['page_path']);
		$result->close();
		$con->close();
		if(isset($_GET['ns']) && $_GET['ns']) {
			$url = $filepath . "api.php?action=query&meta=siteinfo&format=php&siprop=namespaces"; 
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
			curl_setopt($ch, CURLOPT_TIMEOUT, 3); 
			$apires = curl_exec($ch); 
			curl_close($ch);   
			$apires = unserialize($apires);
		}
		if(isset($_GET['ns']) && ctype_digit($_GET['ns']) && isset($apires['query']['namespaces'][$_GET['ns']]['*'])) {
			$ns = $_GET['ns'];
			$nstext = $apires['query']['namespaces'][$_GET['ns']]['*'] . ':';
		} else {
			$ns = 0;
			$nstext = '';
		}
		#var_dump($nstext); 
		$c = '<table id="table" class="tablesorter">
		<thead> 
		<tr> 
			<th>#</th> 
			<th>Title</th>
			<th>Date of page creation</th>
			<th>Page length (in bytes)</th>
			
		</tr> 
		</thead>';

		$dbname = $db . '_p';
		$dbhost = $db . '.labsdb';
		$user = $_GET['user'];
		
		$query = "SELECT
			p.page_title, r.rev_timestamp, p.page_len
		FROM
			revision_userindex AS r
				INNER JOIN page AS p
					ON r.rev_page = p.page_id
		WHERE
			r.rev_parent_id=0 AND r.rev_user_text='$user' AND p.page_is_redirect=0 AND p.page_namespace=$ns;";
			
			
		$con = new mysqli($dbhost, $config['user'], $config['pass'], $dbname);
		if (mysqli_connect_errno()) {
			$content['content'] = "Connect error: " . mysqli_connect_error();
			require_once('template.php');
			exit();
		}
		$result = $con->query($query);
		$num=1;
		while($row = $result->fetch_row()) {
			$c .= "<tr>\n<td>$num</td>\n<td><a target=\"_blank\" href=\"" . $pagepath . urlencode(str_replace(' ', '_', $nstext) . $row[0]) . "\">" . $nstext . str_replace("_", " ", $row[0]) . "</a></td>\n<td>". substr($row[1],0,4) . "-" . substr($row[1],4,2) . "-" . substr($row[1],6,2). "</td>\n<td>{$row[2]}</td>\n</tr>";
			$num++;
		}
		$c .= '</tbody> 
		</table>';
		$content['content'] = $c;
		$result->close();
		$con->close();
		//require_once('list.php');
		//$content['content'] .= $c;
	} else {
		$result->close();
		$con->close();
		$content['content'] = "Unknown database '" . $db . "'";
	}
}
else {
	require_once('list.php');
	$content['content'] = $c;
}
require_once('template.php');
?>
