<?php
/* Return list of namespaces in json format */
if(isset($_GET['wiki'])) {
	require_once('config.php');
	$dbname = 'enwiki_p';
	$dbhost = 'enwiki.labsdb';
	$con = new mysqli($dbhost, $config['user'], $config['pass'], $dbname);
	if (mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		exit();
	}
	$db = $con->real_escape_string($_GET['wiki']);
	$query = "SELECT site_data FROM sites WHERE site_global_key='{$db}';";

	if(!$result = $con->query($query)) {
		print("Error: ");
		print($con->error());
		exit();
	}
	while($row = $result->fetch_row()) {
		$urldata = $row[0];
	}
	$urldata = unserialize($urldata);
	$filepath = str_replace('$1', '', $urldata['paths']['file_path']);
	$result->close();
	$con->close();
	$url = $filepath . "api.php?action=query&meta=siteinfo&format=php&siprop=namespaces"; 
	
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
	curl_setopt($ch, CURLOPT_TIMEOUT, 3); 
	$apires = curl_exec($ch); 
	curl_close($ch);   
	//print($apires);
	$apires = unserialize($apires);
	
	$res = array();
	foreach($apires['query']['namespaces'] as $key=>$value) {
		if($key >= 1) {
			$res[$key] = $value['*'];
		}
	}
	header('Content-Type: application/json');
	echo json_encode($res);
}


?>
