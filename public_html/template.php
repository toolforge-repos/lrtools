<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="/<?php echo $config['tool'];?>/css/main.css" />
<?php echo $content['scripts']; ?>
<title><?php echo $content['title']; ?></title>
</head>
<body>
<div class="wrap">
<div class="topdiv"><span><?php echo $content['title2']; ?></span></div>
<div style="margin:20px;">
<?php echo $content['content']; ?>
</div>
</div>

<div class="footer">
  <span>Powered by <a href="https://tools.wmflabs.org/">Wikimedia Tool Labs</a>. Author <a href="https://uk.wikipedia.org/wiki/User:RLuts">RLuts</a>. <a href="https://uk.wikipedia.org/wiki/User_talk:RLuts?uselang=en">Report a bug</a>. <a href="https://phabricator.wikimedia.org/source/tool-lrtools/">Source</a></span>
</div>
</body>
</html>
